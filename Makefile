CFLAGS := -g -O2 -Wall
LDFLAGS := -g -O2

all: evenodd

evenodd: evenodd.o

clean:
	rm -f evenodd evenodd.o
