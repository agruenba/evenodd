#include <stdio.h>

void even(int i)
{
	printf("%d is even\n", i);
}

void odd(int i)
{
	printf("%d is odd\n", i);
}

int main(void)
{
	int i;
	for (i = 0; ;i++) {
		if (i % 2)
			odd(i);
		else
			even(i);
	}
	return 0;
}
